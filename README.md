# phpMyAdmin updatescript for your shell
This script will update your current phpMyAdmin to the latest version directly from your shell.

## Requirements
- wget
- tar

## Installation
Copy the script to any location of your machine.   

**IMPORTANT: You need to do some configurations, see Settings below.**

### Cronjob

If you want to install a cronjob, run
	
	sh install-cronjob.sh

## Settings

You can set basic settings directly in the `pma-update.sh`, but better use the user oder system wide settings.

### Options

+ `LOCATION` sets the path to `pma-update.sh` (this setting is **mandatory**)
+ `CURRENT`sets the name of the symlink to the current PMA installation, default is `current`
+ `LANGUAGE` of PMA. Leave it blank (default) for all languages or specify a language pack, for example: english
+ `USER` is the owner of the PMA files
+ `GROUP` is the owning group of the PMA files
+ `CTPYE` changes the compression type - "tar.gz" and "tar.bz2" are possible.
+ `LOGLEVEL` set to
	+ `0` for quit mode (no output)
  	+ `1` to output warnings (DEFAULT)
  	+ `2` to output all messages
+ `FORCE` installation if set to `ON`
+ `KEEP` defines how many versions are kept during clean up
  	+ `0` (or below) don't delete any
  	+ `n` >0 keep `n` versions
  
### User based settings 

Instead of changing the settings in the script, you can place the variables in a user based `~/.pma-update.conf` file in the home folder from the user this script runs as.

### System wide settings

Instead of changing the settings in the script or using an user based setting file, you can install a system wide file at `/etc/pma-update.conf`, basing on the sample file `pma-update.sample.conf`.

## Usage
For updating phpMyAdmin to the latest version, execute the shell script like this:

    sh pma-update.sh

If you want to install a specific version

    sh pma-update.sh -r 3.5.0
    
If you want only to clean up any other than the actual version (i.e. by a cron job)

    sh pma-update.sh -d 1
    
    
### More options
    sh pma-update.sh [-hvf] [-r version]  
    -h          this help  
    -v          output all warnings  
    -f          force download, even if this version is installed already  
    -r version  choose a different `version` than the latest
    -d number   delete old versions, but keep `number`