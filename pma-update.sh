#!/bin/bash

##
# PHPMYADMIN UPDATE SCRIPT
# https://bitbucket.org/websomar/pma-updatescript/
# Author: Christoph Jeschke, Stefan Schulz-Lauterbach, Michael Riehemann, Igor Buyanov
##

# SETTINGS
# Please check this settings. Without changing 
# location and pma your installation will not work!
# 
# Instead of changing these values below, 
# you can place them in a .pma-updaterc file in the  
# home folder from the user this script runs as. 
##

CONF_DEFAULT="/etc/pma-update.conf"

if [ -e "$CONF_DEFAULT" ];
then
    echo "Using $CONF_DEFAULT" >&2
    . $CONF_DEFAULT
else
    echo "Using file specifig settings" >&2

    # location of the pma installation
    LOCATION=""

    # path to the current version
    CURRENT="$LOCATION/current"

    # Language of PMA. Leave it blank for all languages or specify a language pack, for example: english
    LANGUAGE=""

    # User of files
    USER=""

    # Group of files
    GROUP=""

    # Compression type. default "tar.gz". tar.bz2 is possible, too.
    CTYPE="tar.gz"

    # set 0 for quiet mode (no output)
    # set 1 to output warnings (DEFAULT)
    # set 2 to output all messages
    LOGLEVEL=2

    # default force to off
    FORCE="off"
    
    # Keep n versions
    # 0 = Keep all versions, default
    # n>0 = Keep n versions
    KEEP=0
fi

# name of the user specific pma rc
CONFIG_FILE=~/.pma-update.conf

# link to the current pma version info
VERSIONLINK="http://www.phpmyadmin.net/home_page/version.php"

################################################
#                                              #
#   DON'T CHANGE ANYTHING FROM HERE            #
#   unless you're into shell scripting         #
#                                              #
################################################

# Output help
usage() {
    echo "usage: sh pma-update.sh [-hvf] [-r version]";
    echo "-h            this help";
    echo "-v            output all warnings";
    echo "-f            force download, even if this version is installed already";
    echo "-r version    choose a different version than the latest.";
    echo "-d number     delete old versions, but keep [number]";
}



# Output warnings
log() {
    if [ "$LOGLEVEL" -gt 0 ]; then
        echo "$@";
    fi
}

# Output additional messages
info() {
    if [ "$LOGLEVEL" -eq 2 ]; then
        echo "$@";
    fi
}

# delete older versions
# if KEEP is set to 0 (or below) skip the cleanup, instead keep n versions, if KEEP n>0
clean_old() 
{
    if [ "$KEEP" -gt 0 ]; then
        log "Cleaning deprecated versions. Keeping $KEEP."
    
        # increase keep due to the calculation done by tail
        let TOKEEP=$KEEP+1
            
        # get the directories who will be deleted
        TODELETE=$(find $LOCATION/ -maxdepth 1 -mindepth 1 -type d -not -path '.'|sort -r |tail -n +$TOKEEP);
        
        # only if we found any directories, we really do the delete
        if [ -n "$TODELETE" ]; then
            rm -rf $TODELETE;
        fi
    else
        log "Skip cleanup, no version will be deleted."
    fi
}

# Options
params="$(getopt -o hvfr:d: -l help --name "$cmdname" -- "$@")"

if [ $? -ne 0 ]; then
    usage
fi

eval set -- "$params"
unset params

while true
do
    case "$1" in
        -v) LOGLEVEL=2;;
        -f) FORCE=on;;
        -r) VERSION="$2"; shift;;
        -h|--help)
            usage
            exit;;
        -d)
            KEEP="$2"
            clean_old
            exit;;
        --)
            shift
            break;;
        *)
            usage;;
    esac
    shift
done

# If user based config exists, load it
if [ -e "$CONFIG_FILE" ]; then
    command . $CONFIG_FILE;
fi

# check if the location is set
if [ -z $LOCATION ]; then
    echo "No location set, so exiting" >&2
    exit 1
else
    echo "Working with pma located at $LOCATION"
fi

# check for wget
if [ ! $(command -v wget) ]; then
    log "wget not found."
    exit 1;
else
    info "wget found at $(command -v wget)"
fi

# check for tar
if [ ! $(command -v tar) ]; then
    log "tar not found."
    exit 1;
else
    info "tar found at $(command -v tar)"
fi

# Check location settings
if [ -z "$LOCATION" ]; then
    log "Please, check your settings. The setting LOCATION is mandatory!";
    exit 1;
fi

# set actual version
if [ "$(command -v readlink)" ]; then
    ACTUAL_VERSION="$(basename $(readlink -f $CURRENT))"
else
    log "readlink is not installed. Aborting."
    exit 1;
fi

# Get the local installed version
if [ -z "$ACTUAL_VERSION" ]; then
    log "Couldn't detect the actual version of your phpMyAdmin installation. Install new instead."
    exit 1;
else
    VERSIONLOCAL=$ACTUAL_VERSION;
    info "Version $ACTUAL_VERSION detected."
fi

# If $USER or $GROUP empty, read from installed phpMyAdmin
if [ -z "$USER" ]; then
    USER=$(stat -c "%U" "$CURRENT/index.php");
fi
if [ -z "$GROUP" ]; then
    GROUP=$(stat -c "%G" "$CURRENT/index.php");
fi

# Check user/group settings
if [ -z "$USER" -o -z "$GROUP" ]; then
    log "Please, check your settings. Set USER and GROUP, please!";
    exit 1;
fi

if [ -z "$LANGUAGE" ]; then
    LANGUAGE="all-languages";
fi

# Get latest version
if [ -n "$VERSION" ]; then

    #Check the versions
    if [ "$VERSION" = "$VERSIONLOCAL" ]; then
        info "phpMyAdmin $VERSIONLOCAL is already installed!";
        
        if [ "$FORCE" != "on" ]; then
            clean_old
            exit 0;
        fi
        
        info "I will install it anyway.";
    fi
    
else

    # Find out latest version
    VERSION=$(wget -q -O /tmp/phpMyAdmin_Update.html $VERSIONLINK && sed -ne '1p' /tmp/phpMyAdmin_Update.html);
    PMA=$VERSION;

    #Check the versions
    if [ "$VERSION" = "$VERSIONLOCAL" ]; then
        info "You have the latest version of phpMyAdmin installed!";
        
        if [ "$FORCE" != "on" ]; then
            clean_old
            exit 0;
        fi
        
        info "I will install it anyway.";
    fi
fi


# Set output parameters
WGETLOG="-q";
VERBOSELOG="";
if [ "$CTYPE" = "tar.gz" ]; then
    TARPARAMS="xzf";
elif [ "$CTYPE" = "tar.bz2" ]; then
    TARPARAMS="xjf";
fi
if [ $LOGLEVEL -eq 2 ]; then
    WGETLOG="-v";
    VERBOSELOG="-v";
    TARPARAMS=${TARPARAMS}v;
fi


# Start update
if [ -n "$VERSION" ];
then
    # download name, only temporary used
    LOCALE_ARCHIVE_NAME="$VERSION.$CTYPE"

    # final target name
    LOCALE_TARGET_NAME="$LOCATION/$VERSION"

    # temporary directory, will be removed
    TMP_PATH="/tmp/pma-$VERSION"

    # downloading archive
    info "Downloading from http://downloads.sourceforge.net/project/phpmyadmin/phpMyAdmin/$VERSION/phpMyAdmin-$VERSION-$LANGUAGE.$CTYPE"
    wget --no-check-certificate $WGETLOG -O $LOCALE_ARCHIVE_NAME --directory-prefix=$LOCATION https://files.phpmyadmin.net/phpMyAdmin/$VERSION/phpMyAdmin-$VERSION-$LANGUAGE.$CTYPE || exit 1

    # ensure there is the target directory
    info "Creating directory $LOCATION/$VERSION"
    mkdir $VERBOSELOG -p $LOCATION/$VERSION || exit 1;

    # extract archive
    info "Makeing temporary directory at $TMP_PATH"
    mkdir $VERBOSELOG -p $TMP_PATH || exit 1;

    info "Extracting to $TMP_PATH ..."
    tar $TARPARAMS $LOCALE_ARCHIVE_NAME -C $TMP_PATH || exit 1;

    # move extracted files to new directory
    info "Moving extracted files to $LOCALE_TARGET_NAME"
    mv $VERBOSELOG $TMP_PATH/*/* $LOCALE_TARGET_NAME/

    # set user rights
    info "Set user and group for $LOCALE_TARGET_NAME to $USER:$GROUP"
    chown $VERBOSELOG -R $USER:$GROUP $LOCALE_TARGET_NAME || exit 1;

    # symlink config
    info "Link config from $LOCATION/config.inc.php to $LOCALE_TARGET_NAME/config.inc.php"
    ln $VERBOSELOG -sf $LOCATION/config.inc.php $LOCALE_TARGET_NAME/config.inc.php || exit 1;

    # update symlink
    info "Move current symlink to $LOCALE_TARGET_NAME"
    rm -rf $VERBOSELOG $CURRENT || exit 1;
    ln $VERBOSELOG -s $LOCALE_TARGET_NAME $CURRENT || exit 1;

    # cleaning up
    info "Cleaning up"
    rm -rf $VERBOSELOG $TMP_PATH $LOCALE_ARCHIVE_NAME || exit 1;
else
    log "Something went wrong while getting the version of phpMyAdmin. :( "
    log "Maybe this link here is dead: $VERSIONLINK";
fi

clean_old